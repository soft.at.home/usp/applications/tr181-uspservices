#!/bin/sh
[ -f /etc/environment ] && source /etc/environment
ulimit -c ${ULIMIT_CONFIGURATION:-0}

case $1 in
    start|boot)
        source /etc/environment
        tr181-uspservices -D
        ;;
    stop)
        if [ -f /var/run/tr181-uspservices.pid ]; then
            kill `cat /var/run/tr181-uspservices.pid`
        fi
        ;;
    debuginfo)
        ubus-cli "USPServices.?"
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    log)
        echo "TODO log tr181 uspservices client"
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|debuginfo|log]"
        ;;
esac
