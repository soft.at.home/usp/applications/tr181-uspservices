# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.3.2 - 2024-04-10(07:16:55 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.3.1 - 2024-03-26(16:20:39 +0000)

## Release v0.3.0 - 2024-03-23(13:10:08 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.2.3 - 2023-10-28(07:20:52 +0000)

### Other

- [CHR2fA] Update Licenses in Network components

## Release v0.2.2 - 2023-09-28(15:39:37 +0000)

### Other

- Opensource component

## Release v0.2.1 - 2023-09-07(12:58:32 +0000)

### Other

- Make component available on gitlab.softathome.com

## Release v0.2.0 - 2023-09-05(14:46:39 +0000)

### New

- [USP] Our uspagent should use the USPServices data model

## Release v0.1.0 - 2023-06-12(07:35:28 +0000)

### New

- Initialize application

